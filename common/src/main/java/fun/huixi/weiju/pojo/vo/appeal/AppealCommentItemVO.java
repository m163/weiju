package fun.huixi.weiju.pojo.vo.appeal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.LocalDateTime;


/**
 *  分页诉求评论 接收参数
 * @Author 叶秋 
 * @Date 2021/11/16 12:00
 * @param 
 * @return 
 **/
@Data
public class AppealCommentItemVO {


    // 评论人信息

    /**
     * 评论人的id
     */
    private Integer userId;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像对应的URL地址
     */
    private String headPortrait;





    // 评论信息

    /**
     * 诉求评论的id
     */
    private Integer appealCommentId;

    /**
     * 诉求id
     */
    private Integer appealId;


    /**
     * 评论的内容
     */
    private String content;

    /**
     * 评论所加的贴图（只能是一张）
     */
    private String url;

    /**
     * 评论的点赞数||赞同数
     */
    private Integer endorseCount;

    /**
     * 对评论评论的次数
     */
    private Integer commentCount;

    /**
     *  评论时间
     **/
    private LocalDateTime createTime;


}
