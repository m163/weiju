package fun.huixi.weiju.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fun.huixi.weiju.pojo.entity.WjAppealTagCenter;

/**
 * <p>
 * 诉求与标志的对应关系 Mapper 接口
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-10
 */
public interface WjAppealTagCenterMapper extends BaseMapper<WjAppealTagCenter> {

}
