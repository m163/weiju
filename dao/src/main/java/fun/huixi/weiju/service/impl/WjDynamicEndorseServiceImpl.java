package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.pojo.entity.WjDynamicEndorse;
import fun.huixi.weiju.mapper.WjDynamicEndorseMapper;
import fun.huixi.weiju.service.WjDynamicEndorseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 动态的点赞表 服务实现类
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjDynamicEndorseServiceImpl extends ServiceImpl<WjDynamicEndorseMapper, WjDynamicEndorse> implements WjDynamicEndorseService {

    @Override
    public Map<Integer, Boolean> queryUserOrEndorseByUserId(Integer userId, List<Integer> dynamicIdList) {
        // 判值
        if (null == userId) {
            return Collections.emptyMap();
        }
        if (CollectionUtils.isEmpty(dynamicIdList)) {
            return Collections.emptyMap();
        }
        List<WjDynamicEndorse> list = this.lambdaQuery()
                .eq(WjDynamicEndorse::getUserId, userId)
                .in(WjDynamicEndorse::getDynamicId, dynamicIdList)
                .list();
        // 获取用户点赞动态ID
        List<Integer> dynamicEndorseIdList = list.stream().map(WjDynamicEndorse::getDynamicId).collect(Collectors.toList());
        // 组装返回结果
        HashMap<Integer, Boolean> result = new HashMap<>(dynamicEndorseIdList.size());
        dynamicIdList.stream().forEach(item -> {
            if (dynamicEndorseIdList.equals(item)) {
                result.put(item, true);
            } else {
                result.put(item, false);
            }
        });
        return result;
    }

    /**
     * 判断用户是否点赞
     *
     * @param userId    用户id
     * @param dynamicId 动态id
     * @return java.lang.Boolean
     * @Author 叶秋
     * @Date 2021/11/24 11:18
     **/
    @Override
    public Boolean judgeUserOrEndorse(Integer userId, Integer dynamicId) {

        Integer count = this.lambdaQuery()
                .eq(WjDynamicEndorse::getUserId, userId)
                .eq(WjDynamicEndorse::getDynamicId, dynamicId)
                .count();

        if(count > 0){
            return true;
        }

        return false;
    }
}
