package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.pojo.entity.WjUserTagCenter;
import fun.huixi.weiju.mapper.WjUserTagCenterMapper;
import fun.huixi.weiju.service.WjUserTagCenterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户与标签的中间表 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjUserTagCenterServiceImpl extends ServiceImpl<WjUserTagCenterMapper, WjUserTagCenter> implements WjUserTagCenterService {

}
