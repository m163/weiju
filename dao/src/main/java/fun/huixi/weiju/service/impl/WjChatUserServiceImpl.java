package fun.huixi.weiju.service.impl;

import fun.huixi.weiju.pojo.entity.WjChatUser;
import fun.huixi.weiju.mapper.WjChatUserMapper;
import fun.huixi.weiju.service.WjChatUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 聊天室对应的用户 服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@Service
public class WjChatUserServiceImpl extends ServiceImpl<WjChatUserMapper, WjChatUser> implements WjChatUserService {

}
