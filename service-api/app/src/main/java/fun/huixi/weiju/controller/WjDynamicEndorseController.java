package fun.huixi.weiju.controller;


import fun.huixi.weiju.business.DynamicBusiness;
import fun.huixi.weiju.service.WjDynamicEndorseService;
import fun.huixi.weiju.util.CommonUtil;
import fun.huixi.weiju.util.wrapper.ResultData;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import fun.huixi.weiju.base.BaseController;

import javax.annotation.Resource;

/**
 * 动态的点赞表 前端控制器
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/wjDynamicEndorse")
public class WjDynamicEndorseController extends BaseController {


    @Resource
    private DynamicBusiness dynamicBusiness;

    @Resource
    private WjDynamicEndorseService dynamicEndorseService;


    /**
     *  动态点赞
     * @Author 叶秋
     * @Date 2021/11/24 11:15
     * @param dynamicId 动态id
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @PostMapping("dynamicEndorse/{dynamicId}")
    private ResultData dynamicEndorse(@PathVariable Integer dynamicId){

        Integer userId = CommonUtil.getNowUserId();

        // 判断用户是否点赞
        Boolean orEndorse = dynamicEndorseService.judgeUserOrEndorse(userId, dynamicId);

        if(orEndorse){
            return ResultData.error("你已经点过赞了");
        }

        dynamicBusiness.endorseDynamic(userId, dynamicId);

        return ResultData.ok();
    }



    /**
     *  取消动态点赞
     * @Author 叶秋
     * @Date 2021/11/29 14:34
     * @param dynamicId 动态id
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @PostMapping("cancelDynamicEndorse/{dynamicId}")
    private ResultData cancelDynamicEndorse(@PathVariable Integer dynamicId){

        Integer userId = CommonUtil.getNowUserId();

        // 判断用户是否点赞
        Boolean orEndorse = dynamicEndorseService.judgeUserOrEndorse(userId, dynamicId);

        if(!orEndorse){
            return ResultData.error("你还未点赞该动态");
        }

        dynamicBusiness.cancelDynamicEndorse(userId, dynamicId);

        return ResultData.ok();
    }





}

