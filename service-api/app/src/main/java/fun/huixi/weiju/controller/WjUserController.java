package fun.huixi.weiju.controller;


import fun.huixi.weiju.business.UserBusiness;
import fun.huixi.weiju.pojo.vo.user.UserRegisterVO;
import fun.huixi.weiju.util.wrapper.ResultData;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import fun.huixi.weiju.base.BaseController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 用户信息表 前端控制器
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/wjUser")
public class WjUserController extends BaseController {

    @Resource
    private UserBusiness userBusiness;


    /**
     *  用户注册
     * @Author 叶秋
     * @Date 2021/11/10 23:26
     * @param userRegisterVO 注册参数
     * @return fun.huixi.weiju.util.wrapper.ResultData
     **/
    @PostMapping("userRegister")
    public ResultData userRegister(@RequestBody @Valid UserRegisterVO userRegisterVO){

        userBusiness.userRegister(userRegisterVO);

        return ResultData.ok();
    }


}

