package fun.huixi.weiju.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import fun.huixi.weiju.base.BaseController;

/**
 * 用户与标签的中间表 前端控制器
 *
 * @author 叶秋
 * @since 2021-11-02
 */
@RestController
@RequestMapping("/wjUserTagCenter")
public class WjUserTagCenterController extends BaseController {

}

